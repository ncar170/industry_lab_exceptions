package ictgradschool.industry.lab_exceptions.ex05;

import ictgradschool.Keyboard;

/**
 * TODO Write a program according to the Exercise Five guidelines in your lab handout.
 */
public class ExerciseFive {

    public void start() throws Exception {
        // TODO Write the codes :)

        String characterlist = null;
        char c;


        try {
            characterlist = getStringFromUser();
            for (int i = 0; i < characterlist.length(); i++) {
                c = characterlist.charAt(i);
                if (c > '0') {
                    throw new InvalidWordException();
                }
            }
        } catch (ExceedMaxStringLengthException e) {
            System.out.println("You entered too many characters.");
        } catch (InvalidWordException e) {
            System.out.println("You entered an invalid word");
        }

        String array1[] = characterlist.split(" ");

        for (String temp: array1) {
            System.out.print(temp.charAt(0) + " ");
        }

    }

    // TODO Write some methods to help you.

    private String getStringFromUser () throws Exception {
        System.out.print("Enter a string of at most 100 characters: ");
        String characterList = Keyboard.readInput();

        if (characterList.length() > 100) {
            throw new ExceedMaxStringLengthException();
        }
        return characterList;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) throws Exception {
        new ExerciseFive().start();
    }
}
