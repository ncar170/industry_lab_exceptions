package ictgradschool;

public class Main {


    public static void main(String[] args) {
        Main p = new Main();
        p.start();
    }

    private void start() {

//        tryCatch01();
//        tryCatch02();
//        tryCatch03();
//        tryCatch04();
//        tryCatch05();
//        tryCatch06();
//        tryCatch07();
//        tryCatch08();
//        throwsClause09();
//        throwsClause10();

    }

    private void tryCatch01() {

        int result = 0;
        int [] nums = null;
        try {
            result = nums.length;
            System.out.println("See you");
        } catch (ArithmeticException e) {
            System.out.println("Problem");
            result = -1;
        }
        System.out.println("Result: " + result);

    }

    private void tryCatch02() {
        int num1 = 120, num2 = 120, result = 0;
        try {
            result = num2 / (num1 - num2);
            System.out.println("Result: " + result);
        } catch (ArithmeticException e) {
            System.out.println("Arithmetic exception");
        }
    }

    private void tryCatch03() {
        int result = 0;
        String[] items = { "one", "two", null };
        try {
            result = items[2].length();
            System.out.println("Result: " + result);
        } catch (NullPointerException e) {
            System.out.println("Null Pointer Exception");
        }

    }

    private void tryCatch04() {
        int num = 0;
        try {
            System.out.println("Enter number: ");
            num = Integer.parseInt(Keyboard.readInput());
            System.out.println("Thank you");
        } catch (NumberFormatException e) {
            System.out.println("Input error");
            num = -1;
        }
        System.out.println("Number: " + num);
    }

    private int tryCatch05() {
        int result = 0;
        int[] nums = { 2, 3, 4, -1, 4 };
        try {
            result = nums[nums[3]];
            System.out.println("See you");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Number error");
            result = -1;
        }
        return result;
    }

    private void tryCatch06() {
        try {
            try06(0, "");
            System.out.println("A");
        } catch (ArithmeticException e) {
            System.out.println("B Error");
        }
    }
    private void try06(int num, String s) {
        System.out.println("C");
        try {
            num = s.length();
            num = 200 / num;
        } catch (NullPointerException e) {
            System.out.println("E Error");
        }
        System.out.println("F");
    }

    private void tryCatch07() {
        try07(0, null);
        System.out.println("A");
    }
    private void try07(int num, String s) {
        System.out.println("B");
        try {
            num = s.length();
        } catch (Exception e) {
            System.out.println("C");
        }
    }

    private void tryCatch08() {
        try {
            try08(0, null);
            System.out.println("A");
        } catch (NullPointerException e) {
            System.out.println("B");
        }
    }
    private void try08(int num, String s) {
        System.out.println("C");
        try {
            num = s.length();
            System.out.println("D");
        } finally {
            System.out.println("E");
        }
        System.out.println("F");
    }

    private void throwsClause09() {
        try {
            throws09(null);
            System.out.println("A");
        } catch (NullPointerException e) {
            System.out.println(e);
        }
        System.out.println("B");
    }
    private void throws09(String numS) throws NullPointerException {
        if (numS == null) {
            throw new NullPointerException("Null String");
        }
        System.out.println("C");
    }

    private void throwsClause10() {
        try {
            throws10(null);
            System.out.println("A");
        } catch (ArithmeticException e) {
            System.out.println(e);
        } finally {
            System.out.println("B");
        }
        System.out.println("C");
    }
    private void throws10(String numS) throws NullPointerException {
        if (numS == null) {
            throw new NullPointerException("Bad String");
        }
        System.out.println("D");
    }

}
